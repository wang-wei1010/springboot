package com.yun.jing.common.config.encryption;


/**
 * 加解密工具类
 *
 * @author gbx
 */
public class EncryptUtils {

    /**
     * 使用SM2密钥解密数据
     *
     * @param sm2DataHex 密文数据
     * @return 明文数据
     */
    public static String getSm2Data(String sm2DataHex, ApiEncryptServer encryptService, String privateKey) {
        ApiEncryptInfoDTO dto = new ApiEncryptInfoDTO();
        dto.setPrivateKey(privateKey);
        dto.setDataHex(sm2DataHex);
        dto.setType("2");
        encryptService.decrypt2Data(dto);
        return dto.getData();
    }

    /**
     * 使用SM2密钥加密数据
     *
     * @param sm2Data 明文数据
     * @return 密文数据
     */
    public static String getSm2DataHex(String sm2Data, ApiEncryptServer encryptService, String publicKey) {
        ApiEncryptInfoDTO dto = new ApiEncryptInfoDTO();
        dto.setPublicKey(publicKey);
        dto.setData(sm2Data);
        dto.setType("2");
        encryptService.encrypt2Data(dto);
        return dto.getDataHex();
    }

    /**
     * 使用SM4密钥解密数据
     *
     * @param sm4DataHex 密文数据
     * @return 明文数据
     */
    public static String getSm4Data(String sm4DataHex, ApiEncryptServer encryptService, String key) {
        ApiEncryptInfoDTO dto = new ApiEncryptInfoDTO();
        dto.setKey(key);
        dto.setDataHex(sm4DataHex);
        dto.setType("4");
        //数据加密
        encryptService.decrypt4Data(dto);
        return dto.getData();
    }

    /**
     * 使用SM4密钥加密数据
     *
     * @param sm4Data 明文数据
     * @return 密文数据
     */
    public static String getSm4DataHex(String sm4Data, ApiEncryptServer encryptService, String key) {
        ApiEncryptInfoDTO dto = new ApiEncryptInfoDTO();
        dto.setKey(key);
        dto.setData(sm4Data);
        dto.setType("4");
        encryptService.encrypt4Data(dto);
        return dto.getDataHex();
    }
}


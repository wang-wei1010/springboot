package com.yun.jing.common.config.encryption;


import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

/**
 * 自定义 DispatcherServlet 来分派 GbxHttpServletRequestWrapper
 */
public class GbxDispatcherServlet extends DispatcherServlet {

    /**
     * 加密功能服务接口类
     */
    private ApiEncryptServer encryptService;

    public GbxDispatcherServlet() {
    }

    public GbxDispatcherServlet(ApiEncryptServer encryptService) {
        this.encryptService = encryptService;
    }

    /**
     * 包装自定义的 request
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @Override
    protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String requestURI = request.getRequestURI();
        // 对符合要求的指定的请求路径接口进行解密操作
        if (StringUtils.isNotBlank(requestURI)) {
            super.doDispatch(new DataEncryptionWrapper(request, encryptService), response);
        } else {
            super.doDispatch(new HttpServletRequestWrapper(request), response);
        }

    }
}


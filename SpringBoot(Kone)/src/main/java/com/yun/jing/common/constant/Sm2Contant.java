package com.yun.jing.common.constant;

/**
 * @author Wang
 * @信息注释
 * @date: 2022/7/28 16:08
 */
public class Sm2Contant {
    //后端公钥与私钥
    public static final String publicKey="04d80a1dbbf505927d87ac8c3798bb1942bc34917383dd55616d936cc03c3dd8cf9983db060adcd0ad49b60a6a292100d2c7e5ce2dd4e0ccf008ece0ba0f486eb5";
    public static final String privateKey="3cd516b0077ea056252fcd4da8171fcf0107adda10b711f153626f7779568ac2";
    //前端公钥
    public static final String customerPublicKey="045e06eb3f942a6ef72526d80448c24ca4c280663299f663578c98cf1049f216c4605112886f3c082caa4b3e9829d68111bc724cf30e7613832b8dacc435403ae7";
}

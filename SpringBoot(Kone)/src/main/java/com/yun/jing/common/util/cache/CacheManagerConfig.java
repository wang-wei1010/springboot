package com.yun.jing.common.util.cache;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * Caffeine配置类
 */
@Configuration
public class CacheManagerConfig<K,V> {
    @Bean
    public Cache<K,V> caffeineCache(){
        return Caffeine.newBuilder()
                .initialCapacity(128)//初始大小
                .maximumSize(2048)//最大数量
                .expireAfterWrite(60, TimeUnit.MINUTES)//过期时间
                .build();
    }
}

package com.yun.jing.common.util.cache;

import org.springframework.stereotype.Component;

/**
 * 缓存接口
 */
@Component
public interface CacheAssemblyA<K,V> {
    /**
     * 缓存删除
     * @param key
     */
    public void delCache(K key);


    /**
     * 缓存更新
     * @param key
     * @param value
     */
    public void upCache(K key,V value);

    /**
     * 添加数据进一级缓存
     * @param key
     * @param value
     */
    public void addCache1(K key,V value);

    /**
     * 添加数据进二级缓存
     * @param key
     * @param value
     */
    public void addCache2(K key,V value);
    /**
     * 获取一级缓存数据
     * @param key
     * @return
     */
    public V getCache1(K key);
    /**
     * 获取二级缓存数据
     * @param key
     * @return
     */
    public V getCache2(K key);

    /**
     * 获取缓存数据
     * @param key
     * @return
     */
    public V getCache(K key);


}

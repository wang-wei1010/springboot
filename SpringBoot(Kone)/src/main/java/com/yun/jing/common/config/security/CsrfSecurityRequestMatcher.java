package com.yun.jing.common.config.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;

/**
 * @author 王巍
 * @信息注释
 * @date: 2022/1/10 13:21
 */
@Configuration
public class CsrfSecurityRequestMatcher implements RequestMatcher {
    private Pattern allowedMethods = Pattern.compile("^(GET|POST|DELETE|PUT|TRACE|OPTIONS)$");

    @Override
    public boolean matches(HttpServletRequest request) {

     if(request.getServletPath().contains("http://127.0.0.1:8088/server/login"))
     {
         return false;
     }

        return !allowedMethods.matcher(request.getMethod()).matches();
    }
}

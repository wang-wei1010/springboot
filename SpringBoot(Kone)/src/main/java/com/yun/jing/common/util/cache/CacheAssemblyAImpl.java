package com.yun.jing.common.util.cache;

import com.github.benmanes.caffeine.cache.Cache;
import com.yun.jing.common.util.redis.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 缓存组件
 */
@Component
public class CacheAssemblyAImpl<K,V> implements CacheAssemblyA<K,V>{

    @Autowired
    public Cache cache;

    @Autowired
    public RedisUtil redisUtil;

    /**
     * 缓存删除
     * @param key
     */
    public void delCache(K key){
    redisUtil.del((String) key);
    cache.invalidate(key);
    }

    /**
     * 缓存更新
     * @param key
     * @param value
     */
    public void upCache(K key,V value){
        int time=(int)(Math.random()*100);
        redisUtil.set((String) key,value,time);
        cache.put(key, value);
    }

    /**
     * 添加数据进一级缓存
     * @param key
     * @param value
     */
    public void addCache1(K key,V value){
        cache.put(key, value);
    }

    /**
     * 添加数据进二级缓存
     * @param key
     * @param value
     */
    public void addCache2(K key,V value){
        int time=(int)(Math.random()*100);
        redisUtil.set((String) key,value,time);
    }

    /**
     * 获取一级缓存数据
     * @param key
     * @return
     */
    public V getCache1(K key){
       return (V) cache.getIfPresent(key);
    }

    /**
     * 获取二级缓存数据
     * @param key
     * @return
     */
    public V getCache2(K key){
        return (V) redisUtil.get((String) key);
    }

    /**
     * 获取缓存数据
     * @param key
     * @return
     */
    public V getCache(K key){
        V object1 = getCache1(key);
        if (object1!=null){
             System.out.println("一级缓存数据读取");
             System.out.println(object1);
            return object1;
        }else {
            System.out.println("二级缓存数据读取");
            Object object = (V) redisUtil.get((String) key);
            if (object==null){
                return null;
            }
            addCache1(key,(V) object);
            return (V)object;
        }
    }

}
